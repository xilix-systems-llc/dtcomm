package poller

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/xilix-systems-llc/dtcomm/plc"
)

type Poller struct {
	plc             *plc.Plc
	Notification    chan VariableNotification
	variablesToPoll []string
	prefix          string
	lock            sync.RWMutex
}

type VariableNotification map[string]float64

func New(plc *plc.Plc, prefix string) *Poller {
	return &Poller{
		plc:          plc,
		Notification: make(chan VariableNotification),
		prefix:       prefix,
	}
}

func (poller *Poller) getPrefixedVars() []string {
	prefixedVariables := make([]string, len(poller.variablesToPoll))
	for i := 0; i < len(poller.variablesToPoll); i++ {
		prefixedVariables[i] = fmt.Sprintf("%v%v", poller.prefix, poller.variablesToPoll[i])
	}
	if poller.prefix != "" {
		log.Info().Msg("")
	}
	return prefixedVariables
}

func (poller *Poller) PollVariables(ctx context.Context, pollTime time.Duration) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	for {
		select {
		case <-ctx.Done():
			return
		default:
			start := time.Now()
			if len(poller.variablesToPoll) > 0 {
				prefixedVariables := poller.getPrefixedVars()
				if variables, err := poller.plc.GetVariables(prefixedVariables...); err == nil {
					localVars := map[string]float64{}
					for key, variable := range variables {
						if variable.Changed {
							variable.Changed = false
							localVars[key[len(poller.prefix):]] = variable.Value
						}
					}
					if len(localVars) > 0 {
						poller.Notification <- localVars
						// select {
						// case poller.Notification <- localVars:
						// default:
						// }
					}
				}
			}
			diff := pollTime - time.Since(start)
			if diff > 0 {
				time.Sleep(diff)
			}
		}
	}
}

func (poller *Poller) AddPollVariables(variableNames ...string) {
	poller.lock.Lock()
	defer poller.lock.Unlock()
	for _, variable := range variableNames {
		if plc.CheckVarRegex.Match([]byte(variable)) {
			variable = strings.ToUpper(variable)
		}
		if !contains(poller.variablesToPoll, variable) {
			poller.variablesToPoll = append(poller.variablesToPoll, variable)
		} else {
			poller.plc.InvalidateVariable(fmt.Sprintf("%v%v", poller.prefix, variable))
		}
	}

}

func (poller *Poller) RemovePollVariable(variableName string) {
	poller.lock.Lock()
	defer poller.lock.Unlock()
	for i, v := range poller.variablesToPoll {
		if v == variableName {
			poller.variablesToPoll = append(poller.variablesToPoll[:i], poller.variablesToPoll[i+1:]...)
		}
	}
}

func contains(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}
