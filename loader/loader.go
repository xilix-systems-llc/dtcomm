package loader

import (
	"bufio"
	"context"
	"fmt"
	"regexp"

	"github.com/rs/zerolog/log"
	"gitlab.com/xilix-systems-llc/dtcomm/comm"
)

var varRegex = regexp.MustCompile("(.*)=([0-9]*)")

type Loader struct {
	comm comm.Comm
}

func (loader *Loader) SendFile(file string) error {
	return loader.comm.SaveFile(file)
}

func New(ctx context.Context, comm comm.Comm) *Loader {
	localLoader := &Loader{
		comm: comm,
	}
	return localLoader
}

func (localLoader *Loader) Load(ctx context.Context) error {
	localCtx, cancel := context.WithCancel(ctx)
	defer cancel()
	localLoader.comm.Start()
	readResp, readDone := localLoader.messageReader(localCtx, localLoader.comm.GetOut())
	errResp, _ := localLoader.messageReader(localCtx, localLoader.comm.GetErr())
	go func() {
		// wg.Add(1)
		// defer wg.Done()
		<-localCtx.Done()
		log.Info().Msg("load done")
		// localLoader.comm.GetIn().Write([]byte{27, 10, 06, 10})
		localLoader.comm.GetIn().Flush()
	}()

	errMsg := ""
	readMsg := ""
	for {
		select {
		case <-localCtx.Done():
			return nil
		case msg := <-errResp:
			log.Info().Str("err resp", msg).Msg("")
			errMsg += msg
		case msg := <-readResp:
			log.Info().Str("read resp", msg).Msg("")
			readMsg += msg
		case <-readDone:
			log.Info().Msg("read done")
			if errMsg != "" {
				return fmt.Errorf("%v", errMsg)
			}
			log.Info().Str("readMsg", readMsg).Msg("read done")
			return nil
		}
	}
}

func (localLoader *Loader) messageReader(ctx context.Context, reader *bufio.Reader) (chan string, chan bool) {
	log.Info().
		Msg("start message reader")
	respMsg := make(chan string)
	done := make(chan bool)
	go func() {
		for {
			select {
			case <-ctx.Done():
				log.Info().
					Msg("done from reader")
				return
			default:
				log.Info().
					Msg("waiting for message")
				msg, err := reader.ReadString('\n')
				if err != nil {
					respMsg <- msg
					log.Info().
						Err(err).
						Str("bytes", msg).
						Msg("message received error")
					done <- true
					return
				}
				log.Info().
					Str("message", string(msg)).
					Msg("notification received")
				respMsg <- msg
			}
		}
	}()
	return respMsg, done
}
