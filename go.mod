module gitlab.com/xilix-systems-llc/dtcomm

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/bramvdbogaerde/go-scp v0.0.0-20200518191442-5c8efdd1d925
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/httplog v0.1.6
	github.com/go-chi/render v1.0.1
	github.com/gobuffalo/here v0.6.2 // indirect
	github.com/markbates/pkger v0.17.0
	github.com/rs/zerolog v1.19.0
	go.uber.org/atomic v1.6.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
