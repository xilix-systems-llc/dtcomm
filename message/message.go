package message

import (
	"bufio"
	"context"
	"sync"

	"github.com/rs/zerolog/log"
	"gitlab.com/xilix-systems-llc/dtcomm/comm"
)

type Message struct {
	responseChan chan string
	Notification chan MessageNotification
	Messages     []string
	writer       *bufio.Writer
	reader       *bufio.Reader
	err          *bufio.Reader
}

type MessageNotification struct {
	Command string
	Value   interface{}
}

type ProgramVariable struct {
	Position    int
	Variable    string
	Description string
	Value       float64
}

func New(
	ctx context.Context,
	wg *sync.WaitGroup,
	comm comm.Comm) *Message {
	comm.Start()
	localMessage := &Message{
		responseChan: make(chan string),
		Notification: make(chan MessageNotification),
		writer:       comm.GetIn(),
		reader:       comm.GetOut(),
		err:          comm.GetErr(),
	}
	localCtx, cancel := context.WithCancel(ctx)
	go localMessage.messageReader(localCtx, wg, localMessage.reader)
	go localMessage.messageReader(localCtx, wg, localMessage.err)
	go func() {
		defer cancel()
		wg.Add(1)
		defer wg.Done()
		<-ctx.Done()
		localMessage.writer.Write([]byte{27, 10, 06, 10})
		localMessage.writer.Flush()
		comm.Close()
	}()
	return localMessage
}

func (message *Message) messageReader(ctx context.Context, wg *sync.WaitGroup, reader *bufio.Reader) {
	log.Info().
		Msg("start message reader")
	for {
		select {
		case <-ctx.Done():
			log.Debug().
				Msg("done from reader")
			return
		default:
			log.Debug().
				Msg("waiting for message")
			msg, err := reader.ReadString(10)
			if err != nil {
				log.Debug().
					Err(err).
					Msg("message received error")
				break
			}
			message.Messages = append(message.Messages, msg)
			message.Notification <- MessageNotification{Command: "SEND", Value: msg}
			log.Debug().
				Str("message", string(msg)).
				Msg("notification received")
		}
	}

}
