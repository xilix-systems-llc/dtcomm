package comm

import (
	"bufio"
)

type Comm interface {
	Close()
	Start() error
	Run() error
	SaveFile(file string) error
	GetIn() *bufio.Writer
	GetOut() *bufio.Reader
	GetErr() *bufio.Reader
}
