package comm

import (
	"bufio"
	"fmt"
	"os/exec"

	"github.com/rs/zerolog/log"
)

type LocalComm struct {
	cmd *exec.Cmd
	in  *bufio.Writer
	out *bufio.Reader
	err *bufio.Reader
}

func (comm *LocalComm) Close() {
}

func (comm *LocalComm) Start() error {
	comm.connect()
	comm.out.Discard(comm.out.Buffered())
	err := comm.cmd.Start()
	if err != nil {
		log.Fatal().
			Msgf("%v", err.Error())
	}
	fmt.Fprintf(comm.in, "\n")
	return nil
}

func (comm *LocalComm) Run() error {
	comm.connect()
	comm.out.Discard(comm.out.Buffered())
	err := comm.cmd.Run()
	if err != nil {
		log.Fatal().
			Msgf("%v", err.Error())
	}
	fmt.Fprintf(comm.in, "\n")
	return nil
}

func (comm *LocalComm) GetIn() *bufio.Writer {
	return comm.in
}

func (comm *LocalComm) GetOut() *bufio.Reader {
	return comm.out
}

func (comm *LocalComm) GetErr() *bufio.Reader {
	return comm.err
}

func (comm *LocalComm) connect() {
	stdout, err := comm.cmd.StdoutPipe()
	if err != nil {
		log.Fatal().
			Msgf("%v", err)
	}
	stdin, err := comm.cmd.StdinPipe()
	if err != nil {
		log.Fatal().
			Msgf("%v", err)
	}
	stderr, err := comm.cmd.StderrPipe()
	if err != nil {
		log.Fatal().
			Msgf("%v", err)
	}
	comm.out = bufio.NewReader(stdout)
	comm.in = bufio.NewWriter(stdin)
	comm.err = bufio.NewReader(stderr)
}

func NewLocalComm(program string) (*LocalComm, error) {
	cmd := exec.Command(program)
	return &LocalComm{
		cmd: cmd,
	}, nil
}
