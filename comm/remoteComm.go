package comm

import (
	"bufio"
	"fmt"
	"strings"

	"github.com/bramvdbogaerde/go-scp"
	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/ssh"
)

type RemoteComm struct {
	in      *bufio.Writer
	out     *bufio.Reader
	err     *bufio.Reader
	session *ssh.Session
	config  *ssh.ClientConfig
	address string
	program string
}

func (comm *RemoteComm) Close() {
	comm.session.Close()
}

func (comm *RemoteComm) Start() error {
	err := comm.connect()
	if err != nil {
		return err
	}
	err = comm.session.Start(comm.program)
	if err != nil {
		return err
	}
	log.Log().
		Str("server", comm.address).
		Str("program", comm.program).
		Msg("started")
	return nil
}

func (comm *RemoteComm) Run() error {
	err := comm.connect()
	if err != nil {
		return err
	}
	err = comm.session.Run(comm.program)
	if err != nil {
		return err
	}
	log.Log().
		Str("server", comm.address).
		Str("program", comm.program).
		Msg("ran")
	return nil
}

func (comm *RemoteComm) GetIn() *bufio.Writer {
	return comm.in
}

func (comm *RemoteComm) GetOut() *bufio.Reader {
	return comm.out
}

func (comm *RemoteComm) GetErr() *bufio.Reader {
	return comm.err
}

func (comm *RemoteComm) SaveFile(file string) error {
	client := scp.NewClient(comm.address, comm.config)
	err := client.Connect()
	if err != nil {
		log.Error().Err(err).Msg("Couldn't establish a connection to the remote server ")
		return nil
	}
	defer client.Close()
	err = client.Copy(strings.NewReader(file), "/var/ftp/usrflash/Temp/temp.dat", "0655", int64(len(file)))

	if err != nil {
		log.Error().Err(err).Msg("Error while copying file ")
	}
	return nil
}
func (comm *RemoteComm) connect() error {
	connection, err := ssh.Dial("tcp", comm.address, comm.config)
	if err != nil {
		return fmt.Errorf("Failed to dial: %s", err)
	}
	session, err := connection.NewSession()
	if err != nil {
		return fmt.Errorf("Failed to create session: %s", err)
	}
	stdin, err := session.StdinPipe()
	if err != nil {
		return fmt.Errorf("Unable to setup stdin for session: %v", err)
	}
	stdout, err := session.StdoutPipe()
	if err != nil {
		return fmt.Errorf("Unable to setup stdout for session: %v", err)
	}
	stderr, err := session.StderrPipe()
	if err != nil {
		return fmt.Errorf("Unable to setup stderr for session: %v", err)
	}
	log.Log().
		Str("server", comm.address).
		Str("program", comm.program).
		Msg("connected")
	comm.session = session
	comm.out = bufio.NewReader(stdout)
	comm.in = bufio.NewWriter(stdin)
	comm.err = bufio.NewReader(stderr)
	if err != nil {
		log.Error().Err(err).Msg("")
	}
	return nil
}

func NewRemoteComm(
	sshConfig *ssh.ClientConfig,
	addr string,
	program string,
) (*RemoteComm, error) {
	return &RemoteComm{
			config:  sshConfig,
			program: program,
			address: addr},
		nil
}
