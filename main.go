//go:generate pkger
package main

import (
	"context"
	"io"
	"net/http"
	"os"
	"os/signal"
	"path"
	"sync"
	"syscall"
	"time"

	"gitlab.com/xilix-systems-llc/dtcomm/broker"
	"gitlab.com/xilix-systems-llc/dtcomm/comm"
	"gitlab.com/xilix-systems-llc/dtcomm/config"
	"gitlab.com/xilix-systems-llc/dtcomm/handlers"
	"gitlab.com/xilix-systems-llc/dtcomm/loader"
	"gitlab.com/xilix-systems-llc/dtcomm/message"
	"gitlab.com/xilix-systems-llc/dtcomm/plc"
	"gitlab.com/xilix-systems-llc/dtcomm/poller"
	"golang.org/x/crypto/ssh"
	"gopkg.in/natefinch/lumberjack.v2"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"github.com/go-chi/httplog"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"github.com/markbates/pkger"
)

func init() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	writers := []io.Writer{}
	writers = append(writers, newRollingFile())
	writers = append(writers, zerolog.ConsoleWriter{Out: os.Stderr})
	multi := zerolog.MultiLevelWriter(writers...)
	log.Logger = zerolog.New(multi).
		With().
		Caller().
		Timestamp().
		Logger()
}

var clientConfig = &ssh.ClientConfig{
	User: "root",
	Auth: []ssh.AuthMethod{
		ssh.Password("deltatau"),
	},
	HostKeyCallback: ssh.InsecureIgnoreHostKey(),
}

func main() {
	config := config.LoadConfiguration("config.json")
	clientConfig.User = config.Username
	clientConfig.Auth = []ssh.AuthMethod{
		ssh.Password(config.Password),
	}
	ctx, cancel := context.WithCancel(context.Background())
	plcComm, err := comm.NewRemoteComm(clientConfig, config.IP, "gpascii -2")
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("unable to connect")
	}
	messageComm, err := comm.NewRemoteComm(clientConfig, config.IP, "sendgetsends/sendgetsends -1")
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("unable to connect")
	}
	fileSendComm, err := comm.NewRemoteComm(clientConfig, config.IP, "gpascii -2 -i/var/ftp/usrflash/Temp/temp.dat")
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("unable to connect")
	}
	wg := sync.WaitGroup{}
	plc := plc.New(ctx, &wg, plcComm)
	plcPoller := poller.New(plc, "")
	plcPoller.AddPollVariables(
		"var1",
		"var2",
		"var3",
		"var4",
		"var5",
	)
	go plcPoller.PollVariables(ctx, (100 * time.Millisecond))
	message := message.New(ctx, &wg, messageComm)
	loader := loader.New(ctx, fileSendComm)
	loadHandler := handlers.NewLoadHandler(loader)
	sseBroker := broker.NewServer(ctx)
	messageHandler := handlers.NewMessageHandler(message, sseBroker)
	plcHandler := handlers.NewPlcHandler(plc, plcPoller, sseBroker)
	kill := make(chan bool)
	r := setupRoutes(plcHandler,
		sseBroker,
		loadHandler,
		messageHandler,
		kill)
	go http.ListenAndServe(":3000", r)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	log.Log().Msg("startup complete")
	select {
	case <-c:
		break
	case <-kill:
		break
	}
	log.Log().Msg("signal caught")
	log.Log().Msg("cancelling")
	cancel()
	log.Log().Msg("wait for waitgroup")
	wg.Wait()
	log.Log().Msg("close comms")
	log.Log().Msg("done")
	time.Sleep(1 * time.Second)
}

func setupRoutes(
	plcHandler *handlers.PlcHandler,
	broker *broker.Broker,
	loadHandler *handlers.LoadHandler,
	messageHandler *handlers.MessageHandler,
	kill chan bool) *chi.Mux {
	r := chi.NewRouter()
	r.Use(cors.AllowAll().Handler)
	r.Use(httplog.RequestLogger(log.Logger))
	r.HandleFunc("/kill", func(w http.ResponseWriter, r *http.Request) {
		kill <- true
	})
	r.HandleFunc("/events", broker.ServeHTTP)
	r.Route("/api/plc", plcHandler.Routes)
	r.Route("/api/load", loadHandler.Routes)
	r.Route("/api/messages", messageHandler.MessageRoutes)
	handlers.FileServer(r, "/", pkger.Dir("/public"))
	return r
}

func newRollingFile() io.Writer {
	return &lumberjack.Logger{
		Filename:   path.Join("./log", "log.json"),
		MaxBackups: 300, // files
		MaxSize:    1,   // megabytes
		MaxAge:     30,  // days
	}
}
