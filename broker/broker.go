package broker

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"go.uber.org/atomic"
)

func NewServer(ctx context.Context) *Broker {
	// Instantiate a broker
	broker := &Broker{
		ctx:        ctx,
		Notifier:   make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
	// Set it running - listening and broadcasting events
	go broker.listen(ctx)
	return broker
}

type Broker struct {
	ctx context.Context

	// Events are pushed to this channel by the main events-gathering routine
	Notifier chan []byte

	// New client connections
	register chan *Client

	// Closed client connections
	unregister chan *Client

	// Client connections registry
	clients map[*Client]bool

	ClientLock sync.Mutex

	NumberOfClients atomic.Uint32
}

type Client struct {
	send chan []byte
}

func (broker *Broker) ServeHTTP(rw http.ResponseWriter, req *http.Request) {

	// Make sure that the writer supports flushing.
	//
	flusher, ok := rw.(http.Flusher)

	if !ok {
		http.Error(rw, "Streaming unsupported!", http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "text/event-stream; charset=utf-8")
	rw.Header().Set("Cache-Control", "no-cache")
	rw.Header().Set("Connection", "keep-alive")
	rw.Header().Set("Access-Control-Allow-Origin", "*")

	// Each connection registers its own message channel with the Broker's connections registry
	client := &Client{send: make(chan []byte)}
	// Signal the broker that we have a new connection
	broker.register <- client
	fmt.Fprintf(rw, "retry: 1000\ndata:\n\n")
	flusher.Flush()
	// Remove this client from the map of connected clients
	// when this handler exits.
	for {
		select {
		case <-req.Context().Done():
			log.Info().
				Msg("req ctx")
			broker.unregister <- client
			return
			//break ForLoop
		case <-broker.ctx.Done():
			log.Info().
				Msg("broker ctx")
			broker.unregister <- client
			return
			//break ForLoop
		case message := <-client.send:
			// Server Sent Events compatible
			fmt.Fprintf(rw, "data: %s\n\n", message)
			// Flush the data immediatly instead of buffering it for later.
			flusher.Flush()
		case <-time.After(3 * time.Second):
			fmt.Fprint(rw, "event: ping\n\n")
			flusher.Flush()
		}

	}
}

func (broker *Broker) listen(ctx context.Context) {
	for {
		select {
		case s := <-broker.register:
			// A new client has connected.
			// Register their message channel
			broker.ClientLock.Lock()
			broker.NumberOfClients.Inc()
			broker.clients[s] = true
			log.Info().
				Uint32("Number Of Clients", broker.NumberOfClients.Load()).
				Msgf("Client added")
			broker.ClientLock.Unlock()
		case s := <-broker.unregister:
			broker.ClientLock.Lock()
			broker.NumberOfClients.Dec()
			// A client has dettached and we want to
			// stop sending them messages.
			delete(broker.clients, s)
			log.Info().
				Uint32("Number Of Clients", broker.NumberOfClients.Load()).
				Msgf("Client removed")
			broker.ClientLock.Unlock()
		case event := <-broker.Notifier:
			// We got a new event from the outside!
			// Send event to all connected clients
			broker.ClientLock.Lock()
			for clientMessageChan := range broker.clients {
				go func(c *Client) {
					select {
					case c.send <- event:
					default:
						break
					}
				}(clientMessageChan)
			}
			broker.ClientLock.Unlock()
		}
	}
}
