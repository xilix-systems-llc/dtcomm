package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/rs/zerolog/log"
	"gitlab.com/xilix-systems-llc/dtcomm/broker"
	"gitlab.com/xilix-systems-llc/dtcomm/plc"
	"gitlab.com/xilix-systems-llc/dtcomm/poller"
)

type plcVariable struct {
	Name  string
	Value string
}

type PlcHandler struct {
	plc    *plc.Plc
	poller *poller.Poller
	broker *broker.Broker
}

func NewPlcHandler(plc *plc.Plc,
	poller *poller.Poller,
	broker *broker.Broker) *PlcHandler {
	handler := &PlcHandler{
		plc:    plc,
		poller: poller,
		broker: broker,
	}
	go handler.handleNotification()
	return handler
}

type ErrResponse struct {
	Err            error `json:"-"` // low-level runtime error
	HTTPStatusCode int   `json:"-"` // http response status code

	StatusText string `json:"status"`          // user-level status message
	AppCode    int64  `json:"code,omitempty"`  // application-specific error code
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}
func ErrRender(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 422,
		StatusText:     "Error rendering response.",
		ErrorText:      err.Error(),
	}
}

func (plcHandler *PlcHandler) Routes(r chi.Router) {
	r.Route("/{variable}", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			variable := chi.URLParam(r, "variable")
			log.Info().
				Str("variable", variable).
				Msgf("Seeking")
			params := r.URL.Query()
			variablesToGet := []string{variable}
			for param := range params {
				variablesToGet = append(variablesToGet, param)
			}

			plcVars, err := plcHandler.plc.GetVariables(variablesToGet...)
			localVariables := make(map[string]float64)
			for key, plcVar := range plcVars {
				localVariables[key] = plcVar.Value
			}

			variableJSON, err := json.Marshal(localVariables)
			if err != nil {
				if err != nil {
					render.Render(w, r, ErrRender(err))
					return
				}
			}
			fmt.Fprintf(w, "%v", string(variableJSON))
		})
		r.Post("/{value}", func(w http.ResponseWriter, r *http.Request) {
			variable := chi.URLParam(r, "variable")
			value := chi.URLParam(r, "value")
			responseNumber, err := strconv.ParseFloat(value, 64)
			if err != nil {
				return
			}
			plcHandler.plc.SetVariable(variable, responseNumber)
		})
	})
}

type VariableNotification struct {
	Command string
	Value   map[string]float64
}

func (plcHandler *PlcHandler) handleNotification() {
	for {
		select {
		case notification := <-plcHandler.poller.Notification:
			variableJSON, err := json.Marshal(VariableNotification{Command: "PLC", Value: notification})
			if err != nil {
				log.Error().Err(err).Msg("plc notification error")
			}
			plcHandler.broker.Notifier <- variableJSON
		}
	}
}
