package handlers

import (
	"io/ioutil"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/xilix-systems-llc/dtcomm/loader"
)

type LoadHandler struct {
	loader *loader.Loader
}

func NewLoadHandler(loader *loader.Loader) *LoadHandler {
	handler := &LoadHandler{loader: loader}
	return handler
}

func (loadHandler *LoadHandler) Routes(r chi.Router) {
	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			render.Render(w, r, ErrRender(err))
		}
		err = loadHandler.loader.SendFile(string(body))

		if err != nil {
			render.Render(w, r, ErrRender(err))
		}
		err = loadHandler.loader.Load(r.Context())
		if err != nil {
			render.PlainText(w, r, err.Error())
		}
	})
}
