package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/rs/zerolog/log"
	"gitlab.com/xilix-systems-llc/dtcomm/broker"
	"gitlab.com/xilix-systems-llc/dtcomm/message"
)

type MessageHandler struct {
	message *message.Message
	broker  *broker.Broker
}

type MessageNotification struct {
	Command string
	Value   []string
}

func NewMessageHandler(message *message.Message, broker *broker.Broker) *MessageHandler {
	handler := &MessageHandler{message: message, broker: broker}
	go handler.handleNotification()
	return handler
}

func (messageHandler *MessageHandler) MessageRoutes(r chi.Router) {
	r.Route("/", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			notificationJSON, err := json.Marshal(messageHandler.message.Messages)
			if err != nil {
				if err != nil {
					render.Render(w, r, ErrRender(err))
					return
				}
			}
			fmt.Fprintf(w, "%v", string(notificationJSON))
		})
	})
}

func (messageHandler *MessageHandler) handleNotification() {
	for {
		select {
		case notification := <-messageHandler.message.Notification:
			variableJSON, err := json.Marshal(notification)
			if err != nil {
				log.Error().Err(err).Msg("bad notification")
			}
			messageHandler.broker.Notifier <- variableJSON
		}
	}
}
