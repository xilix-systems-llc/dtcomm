package plc

import (
	"context"
	"fmt"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/xilix-systems-llc/dtcomm/comm"
	"gitlab.com/xilix-systems-llc/dtcomm/util"
)

var errRegex = regexp.MustCompile(`error(.+)`)
var varRegex = regexp.MustCompile(`([pPmMqQ][0-9]+)=([+-]?[0-9]+\.?[0-9]*([eE][-+]?[0-9]+)?)`)
var CheckVarRegex = regexp.MustCompile(`^(?:&[0-9])?[pPmMqQ][0-9]+$`)

type Plc struct {
	variables       map[string]*PlcVariable
	variablesToPoll []string
	comm            comm.Comm
	responseChan    chan string
	lock            sync.RWMutex
}

type PlcVariable struct {
	UpdatedAt time.Time
	Changed   bool
	Value     float64
}

func New(ctx context.Context, wg *sync.WaitGroup, comm comm.Comm) *Plc {
	err := comm.Start()
	if err != nil {
		log.Fatal().Err(err).Msg("")
	}
	localPlc := &Plc{
		variables:    make(map[string]*PlcVariable),
		responseChan: make(chan string),
		comm:         comm,
	}
	go func() {
		wg.Add(1)
		defer wg.Done()
		<-ctx.Done()
		localPlc.lock.Lock()
		defer localPlc.lock.Unlock()
		comm.GetIn().Write([]byte{27, 10, 06, 10})
		comm.GetIn().Flush()
		comm.Close()
	}()
	return localPlc
}

func (plc *Plc) GetResponse(variable string) (string, error) {
	resp, err := plc.writeRead(variable)
	return resp, err
}

func (plc *Plc) GetVariables(variables ...string) (map[string]*PlcVariable, error) {
	plc.lock.Lock()
	responseVariables := map[string]*PlcVariable{}
	for i, variable := range variables {
		if CheckVarRegex.Match([]byte(variable)) {
			variables[i] = strings.ToUpper(variable)
		}
	}
	plc.lock.Unlock()
	sendString := strings.Join(variables, " ")
	responses, err := plc.readMultiple(sendString, len(variables))
	if err != nil {
		return nil, err
	}
	plc.lock.Lock()
	defer plc.lock.Unlock()
	for i, response := range responses {
		if errRegex.Match([]byte(response)) {
			errorMsg := errRegex.FindStringSubmatch(response)
			delete(plc.variables, variables[i])
			return nil, fmt.Errorf(errorMsg[1])
		}
		if varRegex.Match([]byte(response)) {
			rs := varRegex.FindStringSubmatch(response)
			varName := variables[i]
			value, _ := util.ParseFloat(rs[2])
			log.Debug().
				Str("variable", response).
				Float32("value", float32(value)).
				Msgf("Saved Variable")
			localVariable, ok := plc.variables[varName]
			if ok {
				if localVariable.Value != value {
					localVariable.Changed = true
					localVariable.Value = value
				}
			} else {
				localVariable = &PlcVariable{UpdatedAt: time.Now(), Value: value, Changed: true}
			}
			plc.variables[varName] = localVariable
			responseVariables[varName] = localVariable
		}
	}
	return responseVariables, nil
}

func (plc *Plc) SetVariable(variable string, value float64) error {
	_, err := plc.writeRead(fmt.Sprintf("%v=%v", variable, value))
	return err
}

func (plc *Plc) Write(msg string) error {
	_, err := plc.writeRead(msg)
	return err
}

func (plc *Plc) InvalidateVariable(variable string) error {
	plc.lock.Lock()
	defer plc.lock.Unlock()
	if localVar, ok := plc.variables[variable]; ok {
		localVar.Changed = true
	}
	return nil
}

func (plc *Plc) writeRead(msg string) (string, error) {
	plc.lock.Lock()
	defer plc.lock.Unlock()
	plc.comm.GetIn().WriteString(msg + "\n")
	plc.comm.GetIn().Flush()
	read, _ := plc.comm.GetOut().ReadString(6)
	plc.comm.GetOut().ReadByte()
	log.Debug().Int("length", len(read)).Msg("")
	return read, nil
}

func (plc *Plc) readMultiple(msg string, count int) ([]string, error) {
	plc.lock.Lock()
	defer plc.lock.Unlock()
	responses := []string{}
	stdin := plc.comm.GetIn()
	_, err := stdin.WriteString(msg + "\n")
	if err != nil {
		log.Error().Err(err).Msg("")
		return []string{""}, err
	}
	plc.comm.GetIn().Flush()
	read, err := plc.comm.GetOut().ReadString(6)
	if err != nil {
		log.Error().Err(err).Msg("")
		return []string{""}, err
	}
	plc.comm.GetOut().ReadByte()
	if len(read) < 2 {
		log.Error().Err(err).Msg("")
		return []string{""}, fmt.Errorf("%v", "read multiple less than 2 bytes long")
	}
	read = read[:len(read)-2]
	if len(read) < 2 {
		err, _ := plc.comm.GetErr().ReadString(10)
		err = strings.TrimSpace(err)
		return []string{""}, fmt.Errorf("%v", err)
	}
	log.Debug().
		Bytes("bytes", []byte(read)).
		Msg("")
	lines := strings.Split(read, "\n")
	offset := len(lines) - count
	if offset < 0 {
		return []string{""}, fmt.Errorf("%v", "negative offset")
	}
	responses = lines[offset:]
	log.Debug().
		Int("length", len(read)).
		Msg("")
	return responses, nil
}
